defmodule PlatformWeb.PageControllerTest do
  use PlatformWeb.ConnCase

  test "GET /", %{conn: conn} do
    conn = get conn, "/"
    assert html_response(conn, 200) =~ "Create Player Account"
    assert html_response(conn, 200) =~ "List All Players"
    assert html_response(conn, 200) =~ "Welcome to Phoenix!"
  end
end
